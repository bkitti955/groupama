export default {
  user: {
    name: "Kovács István",
    contractNumber: "87562443",
    lastPayment: "2019. 05. 21.",
    insuranceFeeSettled: "2019. 07. 01."
  },
  payments: [
    {
      id: "EPAYMT87654633",
      paymentMethod: "Átutalás",
      date: "2019. 06. 25",
      amount: "5 000 Ft",
      intervals: [
        {
          insuranceInterval: "2019. 06. 02 - 07. 01.",
          amount: "2 000 Ft",
        },
        {
          insuranceInterval: "2019. 06. 02 - 07. 01.",
          amount: "3 000 Ft",
        }
      ],
      status: "Folyamatban",
      hasAttachment: true,
    },
    {
      id: "123456789",
      contractNumber: "76547894",
      paymentMethod: "Bankkártya",
      date: "2019. 06. 25",
      amount: "3 000 Ft",
      intervals: [
        {
          insuranceInterval: "2019. 06. 02 - 07. 01.",
          amount: "3 000 Ft",
        },

      ],
      status: "Beérkezett",
      hasAttachment: false,
    },
    {
      id: "HDQLAYB8765104",
      contractNumber: "23459145",
      paymentMethod: "Bankkártya",
      date: "2019. 06. 25",
      amount: "7 000 Ft",
      intervals: [
        {
          insuranceInterval: "2019. 06. 02 - 07. 01.",
          amount: "7 000 Ft",
        },

      ],
      status: "Sikertelen",
      hasAttachment: true,
    },
    {
      id: "abc123",
      contractNumber: "42170689",
      paymentMethod: "Átutalás",
      date: "2019. 06. 25",
      amount: "5 000 Ft",
      intervals: [
        {
          insuranceInterval: "2019. 06. 02 - 07. 01.",
          amount: "5 000 Ft",
        },
      ],
      status: "Folyamatban",
      hasAttachment: true,
    }
  ]
};
